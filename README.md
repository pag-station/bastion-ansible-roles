Configuration
-------------

Before starting the playbook, you have to provide an inventory :

```yaml
# file "/production"
all:
  children:
    workstations:
      hosts:
        "<hostname>":
          ansible_connection: "local"
```

Then you have to configure the host's variables in `/host_vars`.

```yaml
# file "/host_vars/<hostname>.yml"
---
desktop_users:
  - my-full-username
toolbox_version: 1.21.9712

```

And then, I can start the playbook :

```shell
sudo ansible-playbook main.yml -i production
```

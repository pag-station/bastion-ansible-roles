docker
=========

Installs docker on debian

Requirements
------------

Role Variables
--------------

_requirements_ is the list of packages to install to be able to install docker
_online_gpg_key_ Where to find the docker gpg key
_fs_gpg_raw_ Where to put downloaded gpg key
_fs_gpg_key_ Where to put the dearmored key

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles: [ docker ]

License
-------

MIT

